//
//  TokenView.h
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/19/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TokenView;

//constants
extern const CGFloat kTokenFieldHeight;

@protocol TokenViewDelegate <NSObject>

@optional
-(void)userDidTapTokenView:(TokenView *)tokenView;

@end


@interface TokenView : UIView

@property (strong, nonatomic) NSString *token;

@property (weak, nonatomic) id <TokenViewDelegate> delegate;

@end
