//
//  WordSuggestionCell.h
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TokenFieldEntry.h"

@interface WordSuggestionCell : UICollectionViewCell

@property (strong, nonatomic) id <TokenFieldEntry> entry;

+(NSString *)reuseIdentifier;

@end
