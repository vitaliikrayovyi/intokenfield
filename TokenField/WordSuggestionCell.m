//
//  WordSuggestionCell.m
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import "WordSuggestionCell.h"

@interface WordSuggestionCell ()

@property (weak, nonatomic) IBOutlet UILabel *wordLabel;

@end

@implementation WordSuggestionCell

+(NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}


-(void)setEntry:(id<TokenFieldEntry>)entry {
    _entry = entry;
    
    _wordLabel.text = entry.title;
}

@end
