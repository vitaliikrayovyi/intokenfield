//
//  WordSuggestionsDataSource.m
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import "WordSuggestionsDataSource.h"
#import "TokenFieldEntry.h"
#import "WordSuggestionCell.h"

@implementation WordSuggestionsDataSource


#pragma mark UICollectionViewDataSource methods

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WordSuggestionCell *cell = nil;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:[WordSuggestionCell reuseIdentifier]
                                                     forIndexPath:indexPath];
    cell.entry = _entries[indexPath.row];
    return cell;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _entries.count;
}


-(void)filterEntries:(NSArray *)initialEntries withText:(NSString *)text {
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id <TokenFieldEntry> evaluatedObject, NSDictionary *bindings) {
        NSString *title = evaluatedObject.title;
        
        if ([title hasPrefix:text]) {
            return YES;
        }
        
        return NO;
    }];
    
    _entries = [initialEntries filteredArrayUsingPredicate:predicate];
}

@end
