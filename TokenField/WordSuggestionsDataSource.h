//
//  WordSuggestionsDataSource.h
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface WordSuggestionsDataSource : NSObject <UICollectionViewDataSource>

@property (strong, nonatomic) NSArray *entries;

-(void)filterEntries:(NSArray *)initialEntries withText:(NSString *)text;

@end
