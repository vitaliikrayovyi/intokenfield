//
//  TokenDataSource.m
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/19/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import "TokenDataSource.h"

@implementation TokenDataSource

-(instancetype)init {
    self = [super init];
    if (self) {
        _tokens = [NSMutableArray new];
    }
    
    return self;
}


-(void)removeTokenWithText:(NSString *)text {
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings) {
        return ![evaluatedObject isEqualToString:text];
    }];
    [_tokens filterUsingPredicate:predicate];
}

#pragma mark - ZFTokenField DataSource

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return kTokenFieldHeight;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField {
    return self.tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    TokenView *tokenView = [[TokenView alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    
    tokenView.token = self.tokens[index];

    return tokenView;
}


@end
