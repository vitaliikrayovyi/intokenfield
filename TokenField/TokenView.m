//
//  TokenView.m
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/19/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import "TokenView.h"


const CGFloat kTokenFieldHeight = 24.0f;
const CGFloat kTokenAdditionalWidth = 40.0f;


@interface TokenView ()

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *tokenLable;
@property (weak, nonatomic) IBOutlet UIView *colorView;

@end

@implementation TokenView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"TokenView" owner:self options:nil];
        [self addSubview:_contentView];
        self.backgroundColor = [UIColor clearColor];
        
        //add gesture recognizer
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        [_contentView addGestureRecognizer:tap];
    }
    return self;
}

-(void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    self.contentView.frame = rect;
}

-(void)setToken:(NSString *)token {
    _token = token;
    [self setupViewWithToken:token];
}


-(void)setupViewWithToken:(NSString *)token {
    _tokenLable.text = token;
    
    CGSize size = [_tokenLable sizeThatFits:CGSizeMake(1000, kTokenFieldHeight)];
    self.frame = CGRectMake(0, 0, size.width + kTokenAdditionalWidth, kTokenFieldHeight);
}


-(void)onTap:(UITapGestureRecognizer *)tap {
    if ([self.delegate respondsToSelector:@selector(userDidTapTokenView:)]) {
        [self.delegate userDidTapTokenView:self];
    }
}

@end
