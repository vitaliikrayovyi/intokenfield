//
//  TokenDataSource.h
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/19/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFTokenField.h"
#import "TokenView.h"

@interface TokenDataSource : NSObject <ZFTokenFieldDataSource>

@property (strong, nonatomic) NSMutableArray *tokens;


-(void)removeTokenWithText:(NSString *)text;


@end
