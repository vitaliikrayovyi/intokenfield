//
//  TagSuggestionInputAccessoryView.h
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TokenFieldEntry.h"

@protocol TagSuggestionViewDelegate <NSObject>

-(void)userDidSelectEntry:(id <TokenFieldEntry>)entry;

@end

@interface TagSuggestionInputAccessoryView : UIView

@property (weak, nonatomic) id <TagSuggestionViewDelegate> delegate;

// initialization
-(instancetype)initWithExistingEntries:(NSArray *)entries;

// update suggestions
-(void)updateWithText:(NSString *)text;

@end
