//
//  ViewController.m
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/19/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import "ViewController.h"
#import "ZFTokenField.h"
#import "TokenDataSource.h"

@interface ViewController () <ZFTokenFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet ZFTokenField *tokenField;
@property (strong, nonatomic) TokenDataSource *tokenDataSource;
@property (weak, nonatomic) TokenView *selectedTokenView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tokenField.dataSource = _tokenDataSource = [TokenDataSource new];
    _tokenField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tokenMarginInTokenInField:(ZFTokenField *)tokenField {
    return 5;
}

- (void)tokenField:(ZFTokenField *)tokenField didReturnWithText:(NSString *)text {
    [self.tokenDataSource.tokens addObject:text];
    [tokenField reloadData];
}

- (void)tokenField:(ZFTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index {
    [self.tokenDataSource.tokens removeObjectAtIndex:index];
}

- (BOOL)tokenFieldShouldEndEditing:(ZFTokenField *)textField {
    return NO;
}


-(void)userDidTapTokenView:(TokenView *)tokenView inTokenField:(ZFTokenField *)tokenField {
    _selectedTokenView = tokenView;
    NSString *message = [NSString stringWithFormat:@"Are you sure you want to delete tag \"%@\"?", tokenView.token];
    [[[UIAlertView alloc] initWithTitle:@"Delete tag"
                                message:message
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Yes", nil] show];
}


#pragma mark UIAlertViewDelegate methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        if (_selectedTokenView) {
            [self.tokenDataSource removeTokenWithText:_selectedTokenView.token];
            [self.tokenField reloadData];
        }
    }
}

@end
