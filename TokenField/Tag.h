//
//  Tag.h
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TokenFieldEntry.h"

@interface Tag : NSObject <TokenFieldEntry>

// TokenFieldEntry protocol
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) UIColor *color;

@end
