//
//  TagSuggestionInputAccessoryView.m
//  TokenField
//
//  Created by Vitalii Krayovyi on 3/20/15.
//  Copyright (c) 2015 Mozidev. All rights reserved.
//

#import "TagSuggestionInputAccessoryView.h"
#import "WordSuggestionCell.h"
#import "WordSuggestionsDataSource.h"

@interface TagSuggestionInputAccessoryView () <UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) WordSuggestionsDataSource *dataSource;

@property (strong, nonatomic) NSArray *existingEntries;

@end

@implementation TagSuggestionInputAccessoryView


#pragma mark Instance initialization

-(instancetype)initWithExistingEntries:(NSArray *)entries {
    self = [super init];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"TagSuggestionInputAccessoryView"
                                      owner:self
                                    options:nil];
        
        [self addSubview:_contentView];
        self.backgroundColor = [UIColor clearColor];
        
        _existingEntries = entries;
        
        [self initialize];
    }
    
    return self;
}


#pragma mark Overriden methods

-(void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    _contentView.frame = rect;
}

-(void)awakeFromNib {
    [self initialize];
}


-(void)initialize {
    [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([WordSuggestionCell class]) bundle:nil]
      forCellWithReuseIdentifier:[WordSuggestionCell reuseIdentifier]];
    
    _dataSource = [WordSuggestionsDataSource new];
    _collectionView.dataSource = _dataSource;
    _collectionView.delegate = self;
}


#pragma mark Interface methods

-(void)updateWithText:(NSString *)text {
    [_dataSource filterEntries:_existingEntries withText:text];
    [_collectionView reloadData];
}


#pragma mark Delegate methods

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(userDidSelectEntry:)]) {
        id <TokenFieldEntry> entry = self.dataSource.entries[indexPath.row];
        [self.delegate userDidSelectEntry:entry];
    }
}

@end
